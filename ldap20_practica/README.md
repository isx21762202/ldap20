# Servidor LDAP
## Javier Moyano - isx21762202
## ASIX - Escola del Treball 2020-2021

Imagen:

* **isx21762202/ldap20:practica** Imagen amb un nou schema master.schema. He afegit alumnes i professors amb els seus corresponent atributs del nou schema (fotos i pdf inclosos)

Detach:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d isx21762202/ldap20:practica   
```

Si s'engega un docker amb phpldapadmin a la vegada, podem mirar interactivament al navegador les noves dades.
