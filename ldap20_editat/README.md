# Servidor LDAP
## Javier Moyano - isx21762202
## ASIX - Escola del Treball 2020-2021

Imagen:

* **isx21762202/ldap20:editat** Imagen base editada a tipo mdb, con un solo fichero ldif (edt.org.ldif), y con passwd de Manager cifrado en SSHA.

Detach:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d isx21762202/ldap20:editat   
```

