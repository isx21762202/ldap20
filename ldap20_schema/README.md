# LDAP Server
## @edt ASIX M06-ASO Curs 2020 - 2021
### Servidor LDAP

* **isx21762202/ldap20:schema** Afegim a la base de dades nous objectClass i creem futbolistes.schema

  Per cada schema s'han definit dades a inserir en la base de dades. 
  S'han fet tres exemples de esquemes de 'futbolistes':

  *  A) objecte structural derivat de inetOrgPerson
  *  B) objecte structural derivat de TOP
  *  C) objecte auxiliar (implementat acompanyant els usuaris actuals inetOrgPerson 
     i posixAccount)


Detach:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d isx21762202/ldap20:schema
```





