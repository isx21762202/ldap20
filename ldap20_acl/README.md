# Servidor LDAP
## Javier Moyano - isx21762202
## ASIX - Escola del Treball 2020-2021

Imagen: 

* **isx21762202/ldap20:acl** Imagen de un servidor ldap para hacer pruebas de modificación de los acl.

Interactivo: 

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it isx21762202/ldap20:acl /bin/bash
```

Detach:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d isx21762202/ldap20:acl
```

