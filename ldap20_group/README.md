# LDAP Server
## @edt ASIX M06-ASO Curs 2020 - 2021
### Servidor LDAP

ASIX M06-ASO Escola del treball de barcelona

Imatge:

* **isx21762202/ldap20:group** Imatge amb usuaris identificats pel uid.


Detach:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d isx21762202/ldap20:group 
```


