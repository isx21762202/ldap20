# Servidor LDAP
## Javier Moyano - isx21762202
## ASIX - Escola del Treball 2020-2021


Imagen:

* **isx2176202/ldap20:base** Imagen base de un servidor ldap que funciona con detach. Carga edt.org, los elementos básicos y los usuarios.

Interactiu:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it isx21762202/ldap20:base /bin/bash
```

Detach:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d isx21762202/ldap20:base 
```

