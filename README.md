# Javier Moyano - isx21762202
# ASIX - Escola del Treball 2020-2021

* **isx2176202/ldap20:base** Imagen base de un servidor ldap que funciona con detach. Carga edt.org, los elementos básicos y los usuarios.

* **isx21762202/ldap20:editat** Imagen base editada a tipo mdb, con un solo fichero ldif (edt.org.ldif), y con passwd de Manager cifrado en SSHA.

* **isx21762202/ldap20:m06cat** Imagen de un servidor ldap que funciona con detach. Carga m06.cat, los elementos básicos y los usuarios.

* **isx21762202/ldap20:acl** Imagen de un servidor ldap para hacer pruebas de modificación de los acl.

* **isx21762202/ldap20:schema** Añadimos a la base de datos nuevos objectClass y creamos futbolistes.schema

* **isx21762202/ldap20:group** Imagen con los usuarios identificados por el uid.

* **isx21762202/ldap20:practica** Imatge amb un nou schema master.schema. He afegit alumnes i professors amb els seus corresponent atributs del nou schema (fotos i pdf inclosos)

* **isx21762202/phpldapadmin:20** Imatge amb un servidor phpldapadmin. Connecta a al servidor ldap anomenat *ldap.edt.org* per accedir a les bases de dades *dc=edt,dc=org* i *cn=config*. Aquesta imatge està basada en fedora:27 per evitar el canvi de sintaxis de PHP 7.4.

