# LDAP Server
## @edt ASIX M06-ASO Curs 2020 - 2021
### Servidor LDAP

ASIX M06-ASO Escola del treball de barcelona

Imatge:

* **isx21762202/phpldapadmin:20** Imatge amb un servidor phpldapadmin. Connecta a al servidor ldap
  anomenat *ldap.edt.org* per accedir a les bases de dades *dc=edt,dc=org* i *cn=config*. Aquesta imatge
  està basada en fedora:27 per evitar el canvi de sintaxis de PHP 7.4.


Detach:
```
$ docker run --rm  --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisix -p 80:80 -d isx21762202/phpldapadmin:20 
